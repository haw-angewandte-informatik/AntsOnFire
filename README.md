# Ants on Fire

Implementierung der Firefly und Ant Colony Algorithmen zur Lösung des Travelling Salesman Problems.  
Zusätzlich die erstellung eines Hybriden Algorithmus, welcher die beiden Algorithmen kombiniert.

# Sample data

Sample data from uwaterloo.ca, source: \
W. Cook. "national traveling salesman problems". uwaterloo.ca. https://www.math.uwaterloo.ca/tsp/world/countries.html (accessed Mai 31, 2023).

| Tsp instance  | known optimum tour length                                             |
|---------------|-----------------------------------------------------------------------|
| qa194         | 9.352                                                                 |
| uy734         | 79.114                                                                |
| wi29          | 27.603                                                                |

# Plan

Präsentation über Google Slides: https://docs.google.com/presentation/d/1FBo9f0UeNR8myvfRtDffLK368JZ2Lp957H_g8NVtI8s/edit?usp=sharing

| Woche | Plan                                                              | Meeting                 |
|-------|-------------------------------------------------------------------|-------------------------|
| 21    | Implementierung, Testung von Parametern, Vorteile, Nachteile etc. | -                       |
| 22    | Implementierung, Testung von Parametern, Vorteile, Nachteile etc. | 04.06. 12.00 Uhr.       |
| 23    | Hybridisierung recherchieren, planen                              | 14.06. im oder nach WP  |
| 24    | Hybridisierung implementieren, testen, auswerten                  | 17.06. 10.00 Uhr        |
| 25    | Vortrag vorbereiten                                               | 25.06. 10.00 Uhr        |
| 26    | Vortrag                                                           | 28.06.                  |
| ...   | ...                                                               |                         |
| 30    | Abgabe Hausarbeit                                                 |                         |


Bis 03./04. Juni:  
  - Implementierung der Algorithmrn in Zweiterteams
    - Firefly: Sandra & Inken
    - Ant Colony: Majid & Marlon
  - Dokumentation der Stärken und Schwächen der Algorithmen
  - Ideen für die Hybridisierung und die Präsentation
  - **Debriefing 04.06. 12 Uhr**

Am 14.06., 
  - Hybridisierung besprechen und planen

Am 17./18. Juni:
  - Treffen zur Besprechung der Hybridisierung (**17.06. 10 Uhr**)
  - Implementierung des Hybriden

Bis 21. Juni:
  - Treffen zur Besprechung der Präsentation?

Am 25. Juni 10 Uhr:
  - Generalprobe/Absprache Präsentation
