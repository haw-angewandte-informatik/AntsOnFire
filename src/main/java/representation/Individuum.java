package representation;

import java.util.ArrayList;
import java.util.List;

/**
 * Implements a Chromosome in a {@link Population}.
 * Represents a solution through a TSP problem by storing the {@link Node}s in
 * order of travel.
 * The starting node (= ending node) is stored only once, but the cost from last
 * node to starting node is included in its cost.
 */
public class Individuum {

    private List<Node> nodes;
    private long cost;
    private Runnable increaseCalcCounter;

    public Individuum() {
        this.nodes = new ArrayList<>();
        this.cost = 0;
    }

    public Individuum(List<Node> nodes, Runnable increaseCalcCounter) {
        this.nodes = nodes;
        this.increaseCalcCounter = increaseCalcCounter;
        calculatePathCost();
    }

    public void setIncreaseCalcCounter(Runnable increaseCalcCounter) {
        this.increaseCalcCounter = increaseCalcCounter;
    }

    /**
     * Fitness of chromosome is higher the smaller the cost of path.
     * 
     * @return fitness value
     */
    public double getFitness() {
        return 1.0 / cost;
    }

    /**
     * The cost of this chromosome's genes.
     * 
     * @return cost value
     */
    public long getCost() {
        return cost;
    }

    public void setCost(long cost){
        this.cost = cost;
    }

    /**
     * Returns the genes of this chromosome in path order.
     * 
     * @return the genes of this chromosome
     */
    public List<Node> getNodes() {
        return nodes;
    }

    /**
     * Adds a new gene to this chromosome. Does NOT calculate cost of path or
     * fitness.
     * 
     * @param node which we want to add
     */
    public void addNode(Node node) {
        nodes.add(node);
    }

    /**
     * Calculates the cost of this chromosome.
     */
    public void calculatePathCost() {
        cost = 0;
        for (int i = 1; i < nodes.size(); i++) {
            cost += nodes.get(i - 1).eucDistance(nodes.get(i));
        }
        cost += nodes.get(nodes.size() - 1).eucDistance(nodes.get(0)); // starting node = ending node
        if (increaseCalcCounter != null)
            increaseCalcCounter.run();
    }

    @Override
    public String toString() {
        return String.valueOf(getCost());
    }

    public void setNodes(List<Node> nodes) {
        this.nodes = new ArrayList<>(nodes);
    }

}
