package representation;

import tsp.TSPData;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

/**
 * Represents a group of Individuums.
 */
public class Population {

    private final int populationSize;
    private List<Individuum> individuums;
    private final Random random;

    public static Population buildRandom(TSPData data, int size){
        List<Individuum> population = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            population.add(new Individuum(data.getRandomTour(), null));
        }
        return new Population(population);
    }

    public Population(List<Individuum> individuums) {
        this.individuums = individuums;
        this.populationSize = individuums.size();
        this.random = new Random();
    }

    /**
     * Returns the best Individuums of this population.
     *
     * @return best Individuum
     */
    public Individuum getBest() {
        individuums.sort(Comparator.comparingLong(Individuum::getCost));
        return individuums.get(0);
    }

    public List<Individuum> getBest(int x){
        individuums.sort(Comparator.comparingLong(Individuum::getCost));
        return individuums.subList(0, x);
    }

    public List<Individuum> getIndividuums() {
        return this.individuums;
    }

    public double getAverageFitness() {
        double totalCost = individuums.stream().mapToDouble(Individuum::getFitness).sum();
        return totalCost / populationSize;
    }

    public double getAverageCost() {
        long totalCost = individuums.stream().mapToLong(Individuum::getCost).sum();
        return totalCost / (double) populationSize;
    }

    public void setNewGeneration(List<Individuum> nextGeneration) {
        this.individuums = nextGeneration;
    }

    public Individuum getRandomIndividuum() {
        return individuums.get(random.nextInt(populationSize));
    }
}
