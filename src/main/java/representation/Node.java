package representation;

import java.util.Objects;

/**
 * Represents a node in the genotype of a {@link Individuum}.
 * Represents a city in a tsp
 */
public record Node(int name, double x, double y) {

    /**
     * Calculates the euclidean distance to another gene.
     *
     * @param node the other city
     * @return distance to other city
     */
    public long eucDistance(Node node) {
        //solution paths in TSPLIB have integer lengths
        return Math.round(Math.sqrt((Math.pow((this.x - node.x), 2) + Math.pow((this.y - node.y), 2))));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Node node = (Node) o;
        return name == node.name && Double.compare(node.x, x) == 0 && Double.compare(node.y, y) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, x, y);
    }

    @Override
    public String toString() {
        return String.format("{Node #%s, x=%s y=%s}", name, x, y);
    }
}
