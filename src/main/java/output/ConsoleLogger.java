package output;

import representation.Population;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ConsoleLogger implements ILogger {
    private int run = 0;
    List<ILogger> nextLoggers = new ArrayList<>();

    @Override
    public void setRun(int run) {
        this.run = run;
        for (ILogger nextLogger : nextLoggers) {
            nextLogger.setRun(run);
        }
    }

    @Override
    public void logGeneration(int generationNumber, Population population, int evalCount) {
        System.out.println(getTimestamp() + "Run " + this.run + ", Generation: " + generationNumber + " Best: "
                + population.getBest().getCost() + " Evaluations: " + evalCount);

        for (ILogger nextLogger : nextLoggers) {
            nextLogger.logGeneration(generationNumber, population, evalCount);
        }
    }

    private static final SimpleDateFormat sdf = new SimpleDateFormat("[HH:mm:ss.SSS] ");

    private String getTimestamp() {
        return sdf.format(new Date());
    }

    @Override
    public void close() {
        for (ILogger nextLogger : nextLoggers) {
            nextLogger.close();
        }
    }

    @Override
    public ILogger andThen(ILogger nextLogger) {
        this.nextLoggers.add(nextLogger);
        nextLogger.setRun(this.run);
        return this;
    }
}
