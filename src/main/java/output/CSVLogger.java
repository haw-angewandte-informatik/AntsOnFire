package output;

import com.opencsv.CSVWriter;
import representation.Population;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CSVLogger implements  ILogger {
    private int run;
    private List<ILogger> nextLoggers = new ArrayList<>();
    private final CSVWriter writer;


    public CSVLogger(String fileName) throws IOException {
        this.writer = new CSVWriter(new FileWriter(fileName, true));
        writer.writeNext(new String[]{"Run", "Generation", "Best Cost", "Worst Cost",
                "Average Cost", "Number of evaluations"});
    }

    /**
     * Logs the given population to file.
     * @param generationNumber the population's generation number
     * @param population the population
     */
    public void logGeneration(int generationNumber, Population population, int evalCount) {
        String[] generation = new String[6];
        generation[0] = String.valueOf(this.run);
        generation[1] = String.valueOf(generationNumber);
        generation[2] = String.valueOf(population.getBest().getCost());
        generation[3] = String.valueOf(population.getIndividuums().get(population.getIndividuums().size()-1).getCost());
        generation[4] = String.valueOf(population.getAverageCost());
        generation[5] = String.valueOf(evalCount);

        writer.writeNext(generation);

        for (ILogger nextLogger : nextLoggers) {
            nextLogger.logGeneration(generationNumber, population, evalCount);
        }
    }

    @Override
    public void close() {
        try {
            writer.close();
            for (ILogger nextLogger : nextLoggers) {
                nextLogger.close();
            }
        } catch (IOException e) {
            //nothing
        }
    }

    public void setRun(int run){
        this.run = run;
        for (ILogger nextLogger : nextLoggers) {
            nextLogger.setRun(run);
        }
    }

    @Override
    public ILogger andThen(ILogger nextLogger) {
        this.nextLoggers.add(nextLogger);
        nextLogger.setRun(this.run);
        return this;
    }
}
