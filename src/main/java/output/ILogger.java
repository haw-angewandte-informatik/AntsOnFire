package output;

import representation.Population;

public interface ILogger extends AutoCloseable {
    public ILogger andThen(ILogger nextLogger);
    void setRun(int run);
    void logGeneration(int generationNumber, Population population, int evalCount);

    void close();
}
