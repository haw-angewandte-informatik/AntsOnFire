package tsp;

import representation.Individuum;
import representation.Node;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Reads and writes Files in the TSPLIB format, which consists of a specification part and a data part.
 */
public class TSPDataParser {

    /**
     * Reads a file in the TSPLIB format und creates a {@link TSPData}.
     *
     * @param fileName name of the file from resources.
     * @return a data set containing the file's TSP data.
     * @throws IOException on I/O problems.
     */
    public TSPData readTSPDataFromFile(String fileName) throws IOException {
        InputStream in = getFileFromResources(fileName);

        String name = "";
        int dimension = 0;

        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        String line = reader.readLine();

        /* Read Specification */
        while (line != null) {
            if (line.contains("NAME")) name = line.replace(" ", "").replace("NAME:", "");
            if (line.contains("DIMENSION"))
                dimension = Integer.parseInt(line.replace(" ", "").replace("DIMENSION:", ""));
            if (line.contains("NODE")) break; // data section begins
            line = reader.readLine();
        }
        List<Node> citiesList = new ArrayList<>();

        /* Read Data */
        line = reader.readLine();
        while (line != null) {
            if (line.equals("EOF")) break;

            String[] splitted = line.split(" "); // data format: "label float float"
            Node node = new Node(Integer.parseInt(splitted[0]), Double.parseDouble(splitted[1]), Double.parseDouble(splitted[2]));
            citiesList.add(node);
            line = reader.readLine();
        }

        return new TSPData(name, dimension, citiesList);
    }

    /**
     * Writes solution paths to file in TSPLIB format.
     *
     * @param set  TSP data set loaded from a .haw.is.tsp file.
     * @param route a path of CityNodes.
     */
    public void writeTSPPathToFile(TSPData set, Individuum route, String path) {
        try (BufferedWriter file = new BufferedWriter(new FileWriter(path))) {
            file.write("NAME: " + set.name() + ".tour");
            file.newLine();
            file.write("COMMENT: Length = " + route.getCost());
            file.newLine();
            file.write("TYPE: TOUR");
            file.newLine();
            file.write("DIMENSION: " + route.getNodes().size());
            file.newLine();
            file.write("TOUR_SECTION");
            file.newLine();
            for (int i = 0; i < route.getNodes().size(); i++) {
                file.write(String.valueOf(route.getNodes().get(i).name()));
                file.newLine();
            }
            file.write("-1");
            file.newLine();
            //file.write("EOF");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Loads a file form resource root into InputStream.
     *
     * @param filename file's name
     * @return InputStream of file
     */
    private InputStream getFileFromResources(String filename) {
        return TSPDataParser.class.getClassLoader().getResourceAsStream(filename);
    }

    /**
     * Creates a {@link Individuum} from a .tour file.
     *
     * @param fileName file's name
     * @param dataSet  TSP data loaded from a tsp file
     * @return  of Nodes
     * @throws IOException on I/O errors
     */
    public Individuum getIndividuumFromFile(String fileName, TSPData dataSet) throws IOException {
        InputStream in = getFileFromResources(fileName);
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        String line = reader.readLine();

        /* Read Specification */
        while (line != null) {
            if (line.contains("TOUR_SECTION")) break; // data section begins
            line = reader.readLine();
        }

        /* Read Data */
        line = reader.readLine();
        Individuum individuum = new Individuum();
        while (!line.equals("-1")) {
            int id = Integer.parseInt(line);
            individuum.addNode(dataSet.getNodeById(id));
            line = reader.readLine();
        }

        return individuum;
    }


}
