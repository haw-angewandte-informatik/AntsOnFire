package tsp;

import representation.Node;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Represents a set of TSP data from a TSPLIB file.
 */
public record TSPData(String name, int dimension, List<Node> nodes) {

    public TSPData {
        if (dimension != nodes.size()) throw new IllegalArgumentException();
    }

    /**
     * Creates a shuffled List of Nodes to get a randomized tour.
     *
     * @return a random tour that visits all genes once.
     */
    public List<Node> getRandomTour() {
        List<Node> copy = new ArrayList<>(nodes);
        Collections.shuffle(copy);

        return copy;
    }

    public Node getNodeById(int id) {
        return nodes.get(id - 1); //ids start at 1 in TSPLIB
    }

    public List<Node> getNodes(){
        return nodes;
    }

    @Override
    public String toString() {
        return "TSPDataSet{" + "name='" + name + '\'' + ", dimension=" + dimension + ", nodes=" + nodes + '}';
    }

}
