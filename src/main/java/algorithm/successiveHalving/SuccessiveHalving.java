package algorithm.successiveHalving;

import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.DoubleStream;

public class SuccessiveHalving {
    private List<Individuum> individuums;

    /**
     * Run the successive halving algorithm. Progress will be logged to the console.
     * 
     * @param hyperParameterCombinations List of hyperparameter combinations. The
     *                                   first value of an individuum will be set by
     *                                   the algorithm.
     * @param iterations                 List of iterations (or generations) to run
     *                                   the algorithm with. Will be set as the
     *                                   first value of an individuum.
     * @param costFunction               Function to calculate the cost of an
     *                                   individuum.
     * @param numberOfWinners            Number of individuums to keep and rank in
     *                                   the end. Ranking will use the last value of
     *                                   the interations list for maximum accuracy.
     * @param logger                     Optional logger function to log the best
     *                                   individuum after each number of iterations
     *                                   completed and the populations is halved.
     * @return List of the best hyperparameter combinations, rank best to worst.
     */
    public List<Individuum> run(List<Individuum> hyperParameterCombinations, DoubleStream iterations,
            Function<Individuum, Double> costFunction, int numberOfWinners, Consumer<Individuum> logger) {
        individuums = hyperParameterCombinations;
        System.out.println(getTimestamp() + " Starting successive halving with " + individuums.size()
                + " candidates");
        List<Double> allIterations = iterations.boxed().toList();
        allIterations.forEach(iteration -> {
            if (individuums.size() <= numberOfWinners) {
                System.out.println(
                        getTimestamp() + " Stopping halving because at most " + numberOfWinners
                                + " individuums are left");
                return;
            }
            System.out.println(getTimestamp() + " Iteration: " + iteration);
            individuums.forEach(combination -> {
                combination.values.set(0, (double) iteration);
                combination.result = costFunction.apply(combination);
            });
            System.out.println(getTimestamp() + " Done running candidates");
            individuums.sort(Comparator.comparingDouble(individuum -> individuum.result));
            individuums = individuums.subList(0, individuums.size() / 2);
            System.out.println(getTimestamp() + " Done running candidates");
            System.out.println(getTimestamp() + " Number of candidates left: " + individuums.size());
            System.out.println(getTimestamp() + " Best result: " + individuums.get(0).result);
            if (logger != null)
                logger.accept(individuums.get(0));
            System.out.println("--------------------");
        });

        // Run the at most three best combinations with the highest number of iterations
        individuums.forEach(combination -> {
            combination.values.set(0, (double) allIterations.stream().max(Double::compareTo).get());
            combination.result = costFunction.apply(combination);
        });
        individuums.sort(Comparator.comparingDouble(individuum -> individuum.result));

        return individuums;
    }

    private static String getTimestamp() {
        return new SimpleDateFormat("[yyyy-MM-dd HH:mm:ss.SSS]").format(new Date());
    }
}
