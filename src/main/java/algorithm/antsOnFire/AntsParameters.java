package algorithm.antsOnFire;

/**
 * Represents the parameters for the ants in the antsOnFire algorithm.
 */
public record AntsParameters(int numberOfAnts, double evaporationCoefficient, double pheromoneWeight, double distanceWeight, boolean elitism, boolean antiStagnation) {
}
