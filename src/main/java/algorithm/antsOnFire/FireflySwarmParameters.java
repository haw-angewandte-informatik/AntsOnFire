package algorithm.antsOnFire;

/**
 * Represents the parameters for the fireflies in the antsOnFire algorithm.
 */
public record FireflySwarmParameters(int numberOfAntsBecomeFireflies, int randomMovementAlpha, int algoRepetitions, boolean elitism) {
}
