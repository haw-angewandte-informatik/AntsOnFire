package algorithm.antsOnFire;

import algorithm.Algorithm;
import output.ILogger;
import representation.Individuum;
import representation.Population;
import tsp.TSPData;

import java.util.HashSet;
import java.util.Random;

import static java.lang.Math.sqrt;

/**
 * AntsOnFire start walking like normal ants, building their tours.
 * They then find the firefly in themselves and fly towards each other.
 * They then add fresh pheromones to their trails and a new generation begins.
 */
public class AntsOnFireAlgorithm implements Algorithm {
    private ILogger logger;
    private final double lowerCostBound;
    private final int maxGenerations;

    private final Random random;
    private final AntsParameters AntsParameters;
    private final FireflySwarmParameters FireFlySwarmParameters;

    /**
     * How many of the best AntsOnFire spread pheromones after moving towards each
     * other (0 for all of them)
     */
    private final int rankBaseFireflySystem;

    private int evalCount = 0;
    private final Runnable incEvalCount = () -> evalCount++;

    public AntsOnFireAlgorithm(double lowerCostBound,
            int maxGenerations,
            AntsParameters antsParameters,
            FireflySwarmParameters fireflySwarmParameters,
            int rankBaseFireflySystem) {
        if (maxGenerations < 1)
            throw new IllegalArgumentException("maxGenerations must be at least 1");
        if (antsParameters.numberOfAnts() < 1)
            throw new IllegalArgumentException("numberOfAnts must be at least 1");
        if (fireflySwarmParameters.numberOfAntsBecomeFireflies() > antsParameters.numberOfAnts())
            throw new IllegalArgumentException("numberOfAntsBecomeFireflies must be at most numberOfAnts");
        if (rankBaseFireflySystem < 0 || rankBaseFireflySystem > antsParameters.numberOfAnts())
            throw new IllegalArgumentException("rankBaseFireflySystem must be between 0 and numberOfAnts");

        this.lowerCostBound = lowerCostBound;
        this.maxGenerations = maxGenerations;
        this.AntsParameters = antsParameters;
        this.FireFlySwarmParameters = fireflySwarmParameters;
        this.rankBaseFireflySystem = rankBaseFireflySystem;
        this.random = new Random();
    }

    @Override
    public void setLogger(ILogger logger) {
        this.logger = logger;
    }

    @Override
    public Individuum run(TSPData input) {
        double absorptionCoefficientGamma = 1.0 / sqrt(input.dimension());
        int generation = 0;
        Population population;
        AntColonyOnFire colony = new AntColonyOnFire(input.getNodes(), FireFlySwarmParameters);
        colony.setIncEvalCount(incEvalCount);
        do {
            colony.init(AntsParameters.pheromoneWeight(), AntsParameters.distanceWeight(),
                    AntsParameters.numberOfAnts(), FireFlySwarmParameters.randomMovementAlpha(),
                    absorptionCoefficientGamma, random);
            colony.buildTours();

            colony.converge();
            adjustPheromones(colony);

            population = colony.getPopulation();
            if (logger != null)
                logger.logGeneration(generation, population, this.evalCount);
            generation++;
        } while (population.getBest().getCost() > lowerCostBound && generation < maxGenerations);

        return population.getBest();
    }

    private Individuum bestIndividuum;
    private double lastCost = 0;
    private int stagnationCounter = 0;

    private void adjustPheromones(AntColonyOnFire colony) {
        final HashSet<AntOnFire> bestAnts = rankBaseFireflySystem > 0
                ? new HashSet<>(colony.getBestAntsOnFire(rankBaseFireflySystem))
                : null;

        colony.getEdges().forEach(edge -> {
            var pheromone = edge.getAttribute("pheromone", Double.class);
            var evaporatedPheromone = (1 - this.AntsParameters.evaporationCoefficient()) * pheromone;

            @SuppressWarnings("unchecked")
            HashSet<AntOnFire> ants = (HashSet<AntOnFire>) edge.getAttribute("ants");
            var freshPheromone = ants.stream()
                    .mapToDouble(ant -> (bestAnts == null || bestAnts.contains(ant)) ? 1.0 / ant.getCost() : 0)
                    .sum();
            ants.clear();

            edge.setAttribute("pheromone", evaporatedPheromone + freshPheromone);
        });

        var currentBest = colony.getPopulation().getBest();
        if (this.bestIndividuum == null || currentBest.getCost() < this.bestIndividuum.getCost()) {
            this.bestIndividuum = currentBest;
        }

        // Elitism for ants!?
        if (this.AntsParameters.elitism()) {
            var numberOfNodes = this.bestIndividuum.getNodes().size();
            for (int i = 0; i < numberOfNodes; i++) {
                var edge = colony.getNode(String.valueOf(this.bestIndividuum.getNodes().get(i % numberOfNodes).name()))
                        .getEdgeBetween(
                                String.valueOf(this.bestIndividuum.getNodes().get((i + 1) % numberOfNodes).name()));
                var currentPheromone = (double) edge.getAttribute("pheromone");
                edge.setAttribute("pheromone", currentPheromone * 1.5);
            }
        }

        if (this.AntsParameters.antiStagnation()) {
            if (this.lastCost == currentBest.getCost()) {
                this.stagnationCounter++;
            } else {
                this.stagnationCounter = 0;
            }

            if (stagnationCounter > 5) {
                colony.getEdges().forEach(edge -> {
                    var pheromone = edge.getAttribute("pheromone", Double.class);
                    var evaporatedPheromone = 0.3 * pheromone;
                    edge.setAttribute("pheromone", evaporatedPheromone);
                });
            }
        }
    }
}
