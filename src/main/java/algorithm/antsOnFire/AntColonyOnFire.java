package algorithm.antsOnFire;

import algorithm.antColony.Ant;
import org.graphstream.graph.Edge;
import org.graphstream.graph.implementations.SingleGraph;
import representation.Individuum;
import representation.Node;
import representation.Population;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AntColonyOnFire {

    private List<AntOnFire> fireAnts = new java.util.ArrayList<>();
    private final SingleGraph map;
    private Runnable incEvalCount;
    private FireflySwarmParameters fireflySwarmParameters;

    public AntColonyOnFire(List<Node> nodes, FireflySwarmParameters fireflySwarmParameters) {
        this.fireflySwarmParameters = fireflySwarmParameters;
        map = new SingleGraph("map");
        for (Node node : nodes) {
            var newNode = map.addNode(String.valueOf(node.name()));
            newNode.setAttribute("x", node.x());
            newNode.setAttribute("y", node.y());
        }

        for (int i = 1; i <= nodes.size(); i++) {
            for (int j = i + 1; j <= nodes.size(); j++) {
                var newEdge = map.addEdge(i + "_" + j, Integer.toString(i), Integer.toString(j));
                newEdge.setAttribute("pheromone", 0d);
                newEdge.setAttribute("ants", new HashSet<Ant>());
            }
        }
    }

    public void setIncEvalCount(Runnable incEvalCount) {
        this.incEvalCount = incEvalCount;
    }

    /**
     * Initializes the ants and their tours.
     *
     * @param pheromoneWeight the weighting to what extent the pheromones should be
     *                        considered.
     * @param distanceWeight  the weighting to what extent the distance should be
     *                        considered.
     * @param antCount        the number of ants in the colony. Not used yet - for
     *                        now, every node gets an ant.
     */
    public void init(double pheromoneWeight, double distanceWeight, int antCount, int randomMovementAlpha,
            double lightAbsorptionGamma, Random random) {
        if (antCount < 1)
            throw new IllegalArgumentException("Must have at least one ant.");
        fireAnts = new ArrayList<>();
        do {
            List<org.graphstream.graph.Node> subListNodes = new ArrayList<>(map.nodes().toList());
            Collections.shuffle(subListNodes);
            subListNodes = subListNodes.subList(0, Math.min(antCount, subListNodes.size()));
            fireAnts.addAll(subListNodes.stream()
                    .map(node -> {
                        var newAnt = new AntOnFire(node, map, pheromoneWeight, distanceWeight, randomMovementAlpha, random,
                                lightAbsorptionGamma);
                        newAnt.setEvalCounter(incEvalCount);
                        return newAnt;
                    })
                    .collect(Collectors.toList()));
            antCount -= subListNodes.size();
        } while (antCount > 0);
        for (var ant : fireAnts) {
            ant.initTour();
        }
    }

    private static final int NUM_THREADS = 4;
    private final ExecutorService executor = java.util.concurrent.Executors.newFixedThreadPool(NUM_THREADS);

    public void buildTours() {
        List<Runnable> batches = new ArrayList<>();
        for (int i = 0; i < NUM_THREADS; i++) {
            int from = i * fireAnts.size() / NUM_THREADS;
            int to = (i + 1) * fireAnts.size() / NUM_THREADS;
            batches.add(() -> {
                for (int j = from; j < to; j++) {
                    AntOnFire ant = fireAnts.get(j);
                    while (!ant.isFinished())
                        ant.step();
                }
            });
        }
        List<Future<?>> futures = batches.stream().map(executor::submit).collect(Collectors.toList());
        try {
            for (var future : futures) {
                future.get();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * provides the population of the ant colony.
     *
     * @return a population of the ant colony.
     */
    public Population getPopulation() {
        return new Population(new ArrayList<>(fireAnts));
    }

    /**
     * Gets the x-best antsOnFire.
     * 
     * @param x the number of best antsOnFire.
     * @return a list of the best antsOnFire.
     */
    public List<AntOnFire> getBestAntsOnFire(int x) {
        sort();
        return this.fireAnts.subList(0, x);
    }

    /**
     * All edges of the environment (graph).
     *
     * @return a stream of all edges of the Ant Colony environment (graph).
     */
    public Stream<Edge> getEdges() {
        return map.edges();
    }

    /**
     * Get a specific node of the environment (graph).
     *
     * @param name the name of the node.
     * @return a stream of all nodes of the Ant Colony environment (graph).
     */
    public org.graphstream.graph.Node getNode(String name) {
        return map.getNode(name);
    }

    private AntOnFire bestFireAnt;

    /**
     * Simulates swarm movement in which every firefly moves towards brighter
     * fireflies
     * or randomly, if it is the brightest of the swarm.
     */
    public void converge() {
        if (fireflySwarmParameters.elitism() && bestFireAnt != null) {
            fireAnts.add(bestFireAnt);
        }
        for (int x = 0; x < fireflySwarmParameters.algoRepetitions(); x++) {
            sort();
            for (int i = 0; i < fireAnts.size(); i++) {
                AntOnFire current = fireAnts.get(i);
                boolean moved = false;
                for (int j = 0; j < i; j++) { // in sorted population: only those before me are brighter than me
                    AntOnFire other = fireAnts.get(j);
                    if (other.getLightIntensity() > current.getLightIntensity()) {
                        current.moveTowards(other);
                        moved = true;
                    }
                }
                if (!moved) { // when I am brighter than everyone else, I move randomly.
                    current.moveRandomly();
                }
            }
        }
        sort();
        this.bestFireAnt = fireAnts.get(0);
        for (AntOnFire fireAnt : fireAnts) {
            fireAnt.adoptInnerFirefly();
            fireAnt.markEdges();
        }
    }

    /**
     * Sorts the population by cost.
     */
    private void sort() {
        fireAnts.sort(Comparator.comparingDouble(AntOnFire::getCost));
    }

    public AntOnFire getBest() {
        fireAnts.sort(Comparator.comparingLong(Individuum::getCost));
        return fireAnts.get(0);
    }
}
