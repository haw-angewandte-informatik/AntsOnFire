package algorithm.antsOnFire;

import algorithm.firefly.Firefly;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;
import representation.Individuum;

import java.util.*;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;


public class AntOnFire extends Individuum {
    private final double pheromoneWeight;
    private final double distanceWeight;
    private final List<Node> tour = new ArrayList<>();
    private final Graph map;
    private Set<Node> remains;
    private final int randomMovementAlpha;
    private boolean tourFinished;
    private Runnable incEvalCounter;
    private Firefly theFireflyInMe;
    private final Random random;
    private final double gamma;

    public AntOnFire(Node startNode, SingleGraph map, double pheromoneWeight, double distanceWeight, int randomMovementAlpha, Random random, double lightAbsorptionGamma) {
        tour.add(startNode);
        this.map = map;
        this.pheromoneWeight = pheromoneWeight;
        this.distanceWeight = distanceWeight;
        this.randomMovementAlpha = randomMovementAlpha;
        this.random = random;
        this.gamma = lightAbsorptionGamma;
    }

    public void adoptInnerFirefly() {
        this.tour.clear();
        List<representation.Node> ffNodes = theFireflyInMe.getNodes();
        for (representation.Node ffNode : ffNodes) {
            tour.add(map.getNode(String.valueOf(ffNode.name())));
        }
    }

    public void setEvalCounter(Runnable incEvalCount) {
        this.incEvalCounter = incEvalCount;
    }

    /**
     * Initializes the tour by adding the start node to the tour.
     */
    public void initTour() {
        remains = map.nodes().collect(Collectors.toSet());
        remains.remove(tour.get(0));
    }

    /**
     * evaluate if the ant is finished.
     * @return true if the ant has visited all nodes, false otherwise.
     */
    public boolean isFinished() {
        if(this.tourFinished) return true;
        if(tour.size() == map.getNodeCount()) {
            // -> Light on fire
            this.theFireflyInMe = new Firefly(getTour(), incEvalCounter);
            return this.tourFinished = true;
        }

        return false;
    }

    /**
     * Based on the desire and distance, the ant decides which node to visit next.
     */
    public void step() {
        Node currentNode = tour.get(tour.size() - 1);
        Node nextNode = decideNextNode(currentNode);
        tour.add(nextNode);
        remains.remove(nextNode);
    }
    /**
     * Makes the fireAnt move towards the brighter fireAnt, if the tour is completed.
     *
     * @param brighterFireAnt   firefly to move towards
     */
    public void moveTowards(AntOnFire brighterFireAnt){
        if(!this.tourFinished)
            throw new IllegalStateException("Tour not finished");

        this.theFireflyInMe.moveTowards(brighterFireAnt.theFireflyInMe, this.random, this.gamma, this.randomMovementAlpha, incEvalCounter);
    }

    /**
     * Moves the fireAnt randomly.
     */
    public void moveRandomly(){
        this.theFireflyInMe.moveRandomly(random, randomMovementAlpha, incEvalCounter);
    }

    /**
     * provides a List of all visited nodes of an ant.
     * @return A list of nodes representing the tour of the ant.
     */
    private List<representation.Node> getTour() {
        return tour.stream().map(node -> new representation.Node(Integer.parseInt(node.getId()),
                (double) node.getAttribute("x"), (double) node.getAttribute("y"))).collect(Collectors.toList());
    }

    /**
     * Every possible step of an ant is collected and by probabilistic selection, the next node is decided.
     * Where nodes with a short distance and many pheromones are more likely to be chosen
     * @param currentNode the node the ant is currently on
     * @return the next node to visit
     */
    private Node decideNextNode(Node currentNode) {
        Map<Node, Double> desires = remains.stream()
                .collect(Collectors.toMap(node -> node, node -> desire(currentNode, node)));
        var desireSum = desires.values().stream().reduce(0.0, Double::sum);
        var probabilities = desires.entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey, entry -> entry.getValue() / desireSum));

        Map<Node, Double> ranking = probabilities.entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey, entry -> entry.getValue() * Math.random()));

        return ranking.entrySet().stream().max(Map.Entry.comparingByValue()).get().getKey();
    }

    /**
     * Calculates the euclidian distance between two nodes.
     * @param n1 the first node
     * @param n2 the second node
     * @return the euclidian distance between the two nodes
     */
    private static double getEuclidicDistance(Node n1, Node n2) {
        double x1 = (double) n1.getAttribute("x");
        double y1 = (double) n1.getAttribute("y");
        double x2 = (double) n2.getAttribute("x");
        double y2 = (double) n2.getAttribute("y");
        return Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
    }

    /**
     * Calculates the desire of an ant to visit a node by evaluate the distance between two nodes and
     * the pheromones on the edge between them.
     * @param currentNode the node the ant is currently on
     * @param nextNode the node the ant wants to visit
     * @return the desire of the ant to visit the next node
     */
    private double desire(Node currentNode, Node nextNode) {
        var tau = (double) currentNode.getEdgeBetween(nextNode).getAttribute("pheromone");
        var ny = 1 / getEuclidicDistance(currentNode, nextNode);

        return Math.pow(tau, pheromoneWeight) * Math.pow(ny, distanceWeight);
    }

    public void markEdges() {
        for (int i = 0; i < this.tour.size(); i++) {
            markEdgeForPheromone(tour.get(i), tour.get((i + 1) % tour.size()));
        }
    }

    /**
     * Marks the edge between two nodes for pheromone update.
     * @param currentNode the node the ant is currently on
     * @param nextNode the node the ant wants to visit
     */
    private void markEdgeForPheromone(Node currentNode, Node nextNode) {
        var edge = currentNode.getEdgeToward(nextNode);
        @SuppressWarnings("unchecked")
        var set = (HashSet<AntOnFire>) edge.getAttribute("ants");
        mutex.lock();
        set.add(this);
        mutex.unlock();
    }

    private static final ReentrantLock mutex = new ReentrantLock();

    /**
     * Returns the fireflies' brightness which is equal to an individuum's fitness
     *
     * @return light intensity
     */
    public double getLightIntensity() {
        return theFireflyInMe.getFitness();
    }

    @Override
    public long getCost() {
        return theFireflyInMe.getCost();
    }
}
