package algorithm;

import output.ILogger;
import representation.Individuum;
import tsp.TSPData;

public interface Algorithm {
    void setLogger(ILogger logger);

    /**
     * Runs the algorithm and returns the best individuum found.
     * @input The geodata to run the algorithm on.
     * @return The best individuum found.
     */
    Individuum run(TSPData input);
}
