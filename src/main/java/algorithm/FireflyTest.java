package algorithm;

import algorithm.firefly.FireflyAlgorithm;
import output.CSVLogger;
import output.ILogger;
import tsp.TSPData;
import tsp.TSPDataParser;

import java.io.IOException;

public class FireflyTest {
    public static final String SET_734 = "uy734.tsp";
    public static final String SET_194 = "qa194.tsp";
    public static final String SET_29 = "wi29.tsp";

    public static final int RUNS = 10;

    public static void main(String[] args) throws IOException {

        TSPDataParser parser = new TSPDataParser();

        TSPData data = parser.readTSPDataFromFile(SET_194);
        CSVLogger logger = new CSVLogger(String.format("firefly_%s.csv", SET_194.split("\\.")[0]));
        double sum = 0;
        long result;
        Algorithm algo;
        for (int i = 0; i < RUNS; i++) {
            logger.setRun(i);
            algo = new FireflyAlgorithm(100, 1, 1000);
            algo.setLogger(logger);
            result = algo.run(data).getCost();
            sum += result;
            System.out.printf("RUN: %s, result: %s%n", i, result);
        }
        logger.close();
        System.out.println("Erg:" + sum / RUNS);
    }

}
