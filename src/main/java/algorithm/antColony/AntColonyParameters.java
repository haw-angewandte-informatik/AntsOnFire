package algorithm.antColony;

import java.util.Arrays;
import java.util.List;

/**
 * Represents the parameters for the ant colony algorithm.
 * Version 1.0
 * Date: 2021-06-03
 * Author: Marlon Regenhardt, Majid Moussa Adoyi
 */
public record AntColonyParameters(int maxGenerations, double lowerCostBound, int numberOfAnts, double evaporationCoefficient, double pheromoneWeight, double distanceWeight, boolean useElitism, int rankBasedAntSystem) {
    public static AntColonyParameters fromList(List<Double> list){
        if(list.size() != 8){
            throw new IllegalArgumentException("Expected 8 parameters for ant colony algorithm.");
        }
        return new AntColonyParameters(list.get(0).intValue(), list.get(1), list.get(2).intValue(), list.get(3), list.get(4), list.get(5), list.get(6) > 0.5, list.get(7).intValue());
    }

    public List<Double> getList(){
        return Arrays.asList((double)maxGenerations, lowerCostBound, (double)numberOfAnts, evaporationCoefficient, pheromoneWeight, distanceWeight, useElitism?1.0:0.0, (double)rankBasedAntSystem);
    }
}
