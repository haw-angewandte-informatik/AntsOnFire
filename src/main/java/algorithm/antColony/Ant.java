package algorithm.antColony;

import java.util.*;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;

/**
 * An ant is a single agent in the ant colony algorithm. It has a tour on the
 * map (Graph) and can decide which node to visit next.
 * Ant decides between the nodes based on the pheromone and distance of the
 * edges.
 * Version 1.0
 * Date: 20223-06-03
 * Author: Marlon Regenhardt, Majid Moussa Adoyi
 */
public class Ant {
    public double pheromoneWeight;
    public double distanceWeight;
    private final List<Node> tour = new ArrayList<>();
    private final Graph map;
    private Set<Node> remains;
    private Runnable incEvalCount;
    private boolean tourFinished = false;
    private long cost;

    public Ant(Node startNode, Graph map, double pheromoneWeight, double distanceWeight) {
        tour.add(startNode);
        this.map = map;
        this.pheromoneWeight = pheromoneWeight;
        this.distanceWeight = distanceWeight;
    }

    public void set(Runnable incEvalCount) {
        this.incEvalCount = incEvalCount;
    }

    /**
     * Initializes the tour by adding the start node to the tour.
     */
    public void initTour() {
        remains = map.nodes().collect(Collectors.toSet());
        remains.remove(tour.get(0));
    }

    /**
     * evaluate if the ant is finished.
     * 
     * @return true if the ant has visited all nodes, false otherwise.
     */
    public boolean isFinished() {
        if (tourFinished)
            return true;
        if (tour.size() == map.getNodeCount()) {
            tourFinished = true;
            double sum = 0;
            for (int i = 0; i < tour.size(); i++) {
                sum += getEuclidicDistance(tour.get(i), tour.get((i + 1) % tour.size()));
            }
            if (incEvalCount != null)
                incEvalCount.run();
            cost = (long) sum;
            return true;
        }

        return false;
    }

    /**
     * Based on the desire and distance, the ant decides which node to visit next.
     */
    public void step() {
        Node currentNode = tour.get(tour.size() - 1);
        Node nextNode = decideNextNode(currentNode);
        markEdgeForPheromone(currentNode, nextNode);
        tour.add(nextNode);
        remains.remove(nextNode);
    }

    /**
     * provides a List of all visited nodes of an ant.
     * 
     * @return A list of nodes representing the tour of the ant.
     */
    public List<representation.Node> getTour() {
        return tour.stream().map(node -> new representation.Node(Integer.parseInt(node.getId()),
                (double) node.getAttribute("x"), (double) node.getAttribute("y"))).collect(Collectors.toList());
    }

    /**
     * Every possible step of an ant is collected and by probabilistic selection,
     * the next node is decided.
     * Where nodes with a short distance and many pheromones are more likely to be
     * chosen
     * 
     * @param currentNode the node the ant is currently on
     * @return the next node to visit
     */
    private Node decideNextNode(Node currentNode) {
        Map<Node, Double> desires = remains.stream()
                .collect(Collectors.toMap(node -> node, node -> desire(currentNode, node)));
        var desireSum = desires.values().stream().reduce(0.0, Double::sum);
        var probabilities = desires.entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey, entry -> entry.getValue() / desireSum));

        Map<Node, Double> ranking = probabilities.entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey, entry -> entry.getValue() * Math.random()));

        return ranking.entrySet().stream().max(Map.Entry.comparingByValue()).get().getKey();
    }

    /**
     * Calculates the euclidian distance between two nodes.
     * 
     * @param n1 the first node
     * @param n2 the second node
     * @return the euclidian distance between the two nodes
     */
    private static double getEuclidicDistance(Node n1, Node n2) {
        double x1 = (double) n1.getAttribute("x");
        double y1 = (double) n1.getAttribute("y");
        double x2 = (double) n2.getAttribute("x");
        double y2 = (double) n2.getAttribute("y");
        return Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
    }

    /**
     * Calculates the desire of an ant to visit a node by evaluate the distance
     * between two nodes and
     * the pheromones on the edge between them.
     * 
     * @param currentNode the node the ant is currently on
     * @param nextNode    the node the ant wants to visit
     * @return the desire of the ant to visit the next node
     */
    private double desire(Node currentNode, Node nextNode) {
        var tau = (double) currentNode.getEdgeBetween(nextNode).getAttribute("pheromone");
        var ny = 1 / getEuclidicDistance(currentNode, nextNode);

        return Math.pow(tau, pheromoneWeight) * Math.pow(ny, distanceWeight);
    }

    /**
     * Marks the edge between two nodes for pheromone update.
     * 
     * @param currentNode the node the ant is currently on
     * @param nextNode    the node the ant wants to visit
     */
    private void markEdgeForPheromone(Node currentNode, Node nextNode) {
        var edge = currentNode.getEdgeToward(nextNode);
        @SuppressWarnings("unchecked")
        var set = (HashSet<Ant>) edge.getAttribute("ants");
        
        mutex.lock();
        set.add(this);
        mutex.unlock();
    }
    
    private static final ReentrantLock mutex = new ReentrantLock(false);

    /**
     * returns the total cost of the tour of the ant.
     * 
     * @return total cost of tour
     */
    public long getCost() {
        return cost;
    }
}
