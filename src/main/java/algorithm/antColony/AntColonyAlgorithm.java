package algorithm.antColony;

import java.util.*;

import algorithm.Algorithm;
import output.ILogger;
import representation.Individuum;
import representation.Population;
import tsp.TSPData;

/**
 * Implementation of the Ant Colony Optimization algorithm with some
 * optimizations strategies.
 * version 1.0
 * Date: 20223-06-03
 * Author: Marlon Regenhardt, Majid Moussa Adoyi
 */

public class AntColonyAlgorithm implements Algorithm {
    private ILogger logger;
    private final double lowerCostBound;
    private final int maxGenerations;
    private final int numberOfAnts;
    private double evaporationCoefficient = 0.5;
    private double pheromoneWeight = 1.0;
    private double distanceWeight = 1.0;
    private final boolean elitism;
    private int rankBaseAntSystem = 0;
    private int evalCount = 0;
    private final Runnable incEvalCount = () -> evalCount++;

    /**
     * Initializes the algorithm with the given parameters.
     * 
     * @param lowerCostBound         Lower tour length threshold for terminating the
     *                               algorithm.
     * @param numberOfAnts           Number of ants to use.
     * @param maxGenerations         Maximum number of generations to run the
     *                               algorithm.
     * @param evaporationCoefficient Evaporation coefficient for pheromones.
     * @param pheromoneWeight        Weight of pheromones in the decision rule.
     * @param distanceWeight         Weight of distance in the decision rule.
     * @param useElitism             Whether to use elitism.
     * @param useRankBaseAntSystem   Whether to use rank based ant system.
     */
    public AntColonyAlgorithm(
            double lowerCostBound,
            int numberOfAnts,
            int maxGenerations,
            double evaporationCoefficient,
            double pheromoneWeight,
            double distanceWeight,
            boolean useElitism,
            int useRankBaseAntSystem) {
        this.lowerCostBound = lowerCostBound;
        this.numberOfAnts = numberOfAnts;
        this.maxGenerations = maxGenerations;
        this.evaporationCoefficient = evaporationCoefficient;
        this.pheromoneWeight = pheromoneWeight;
        this.distanceWeight = distanceWeight;
        this.elitism = useElitism;
        this.rankBaseAntSystem = useRankBaseAntSystem;
        if (rankBaseAntSystem < 0 || rankBaseAntSystem > numberOfAnts) {
            throw new IllegalArgumentException("rankBaseAntSystem must be between 0 and numberOfAnts");
        }
    }

    @Override
    public void setLogger(ILogger logger) {
        this.logger = logger;
    }

    @Override
    public Individuum run(TSPData input) {
        int generation = 0;
        Population population;
        try (AntColony colony = new AntColony(input.getNodes())) {
            colony.setIncEvalCount(incEvalCount);
            do {
                colony.init(pheromoneWeight, distanceWeight, numberOfAnts);

                long start = System.currentTimeMillis();
                colony.buildTours();
                long end = System.currentTimeMillis();
                System.out.println("Time: " + (end - start) + "ms");
                adjustPheromones(colony);

                population = colony.getPopulation();
                if (logger != null)
                    logger.logGeneration(generation, population, this.evalCount);
                generation++;
            } while (population.getBest().getCost() > lowerCostBound && generation < maxGenerations);
            return population.getBest();
        }
        catch (Exception exception) {
            exception.printStackTrace();
            return null;
        }
    }

    private Individuum bestIndividuum;

    /**
     * After each generation, the pheromones are adjusted. This means that the
     * pheromones have to be evaporated and
     * fresh pheromones have to be added. The amount of fresh pheromones is
     * determined by the ants that have found a
     * tour shorter than the best tour found so far.
     * If the ranked base ant system is used, only the best ants are considered.
     * If elitism is used, the best ant overall adds extra pheromones to the edges
     * of its tour.
     * 
     * @param colony the colony to adjust the pheromones of
     */
    private void adjustPheromones(AntColony colony) {
        final HashSet<Ant> bestAnts = rankBaseAntSystem > 0 ? new HashSet<>(colony.getBestAnts(rankBaseAntSystem))
                : null;

        colony.getEdges().forEach(edge -> {
            var pheromone = edge.getAttribute("pheromone", Double.class);
            var evaporatedPheromone = (1 - evaporationCoefficient) * pheromone;

            @SuppressWarnings("unchecked")
            HashSet<Ant> ants = (HashSet<Ant>) edge.getAttribute("ants");
            var freshPheromone = ants.stream()
                    .mapToDouble(ant -> (bestAnts == null || bestAnts.contains(ant)) ? 1.0 / ant.getCost() : 0).sum();
            ants.clear();

            edge.setAttribute("pheromone", evaporatedPheromone + freshPheromone);
        });

        var currentBest = colony.getPopulation().getBest();
        if (this.bestIndividuum == null || currentBest.getCost() < this.bestIndividuum.getCost()) {
            this.bestIndividuum = currentBest;
        }

        // Elitism
        if (this.elitism) {
            var numberOfNodes = this.bestIndividuum.getNodes().size();
            for (int i = 0; i < numberOfNodes; i++) {
                var edge = colony.getNode(String.valueOf(this.bestIndividuum.getNodes().get(i % numberOfNodes).name()))
                        .getEdgeBetween(
                                String.valueOf(this.bestIndividuum.getNodes().get((i + 1) % numberOfNodes).name()));
                var currentPheromone = (double) edge.getAttribute("pheromone");
                edge.setAttribute("pheromone", currentPheromone * 1.5);
            }
        }
    }
}
