package algorithm.antColony;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.graphstream.graph.Edge;
import org.graphstream.graph.implementations.SingleGraph;

import representation.Individuum;
import representation.Node;
import representation.Population;

/**
 * Represents a colony of ants and their environment in form of a graph.
 * Version 1.0
 * Date: 2021-06-03
 * Author: Marlon Regenhardt, Majid Moussa Adoyi
 */
public class AntColony implements AutoCloseable {
    private List<Ant> ants = new java.util.ArrayList<>();
    private final SingleGraph map;
    private Runnable incEvalCount;

    public AntColony(List<Node> nodes) {
        map = new SingleGraph("map");
        for (Node node : nodes) {
            var newNode = map.addNode(String.valueOf(node.name()));
            newNode.setAttribute("x", node.x());
            newNode.setAttribute("y", node.y());
        }

        for (int i = 1; i <= nodes.size(); i++) {
            for (int j = i + 1; j <= nodes.size(); j++) {
                var newEdge = map.addEdge(i + "_" + j, Integer.toString(i), Integer.toString(j));
                newEdge.setAttribute("pheromone", 0d);
                newEdge.setAttribute("ants", new HashSet<Ant>());
            }
        }
    }

    public void setIncEvalCount(Runnable incEvalCount) {
        this.incEvalCount = incEvalCount;
    }

    /**
     * Initializes the ants and their tours.
     * 
     * @param pheromoneWeight the weighting to what extent the pheromones should be
     *                        considered.
     * @param distanceWeight  the weighting to what extent the distance should be
     *                        considered.
     * @param antCount        the number of ants in the colony. If there are more
     *                        ants than nodes, every node will get at least one ant.
     */
    public void init(double pheromoneWeight, double distanceWeight, int antCount) {
        if (antCount < 1)
            throw new IllegalArgumentException("Must have at least one ant.");
        ants = new ArrayList<>();
        do {
            List<org.graphstream.graph.Node> subListNodes = new ArrayList<>(map.nodes().toList());
            Collections.shuffle(subListNodes);
            subListNodes = subListNodes.subList(0, Math.min(antCount, subListNodes.size()));
            ants.addAll(subListNodes.stream()
                    .map(node -> {
                        var newAnt = new Ant(node, map, pheromoneWeight, distanceWeight);
                        newAnt.set(incEvalCount);
                        return newAnt;
                    })
                    .collect(Collectors.toList()));
            antCount -= subListNodes.size();
        } while (antCount > 0);
        for (var ant : ants) {
            ant.initTour();
        }
    }

    private static final int NUM_THREADS = 8;
    private final ExecutorService executor = java.util.concurrent.Executors.newFixedThreadPool(NUM_THREADS);

    public void buildTours() {
        List<Runnable> batches = new ArrayList<>();
        for (int i = 0; i < NUM_THREADS; i++) {
            int from = i * ants.size() / NUM_THREADS;
            int to = (i + 1) * ants.size() / NUM_THREADS;
            batches.add(() -> {
                for (int j = from; j < to; j++) {
                    Ant ant = ants.get(j);
                    while (!ant.isFinished())
                        ant.step();
                }
            });
        }
        List<Future<?>> futures = batches.stream().map(executor::submit).collect(Collectors.toList());
        try {
            for (var future : futures) {
                future.get();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * provides the population of the ant colony.
     * 
     * @return a population of the ant colony.
     */
    public Population getPopulation() {
        return new Population(ants.stream().map(ant -> {
            var ind = new Individuum();
            ind.setNodes(ant.getTour());
            ind.setCost(ant.getCost());
            return ind;
        })
                .collect(Collectors.toList()));
    }

    public List<Ant> getBestAnts(int x) {
        this.ants.sort(Comparator.comparingDouble(Ant::getCost));
        return this.ants.subList(0, x);
    }

    /**
     * All edges of the environment (graph).
     * 
     * @return a stream of all edges of the Ant Colony environment (graph).
     */
    public Stream<Edge> getEdges() {
        return map.edges();
    }

    /**
     * Get a specific node of the environment (graph).
     * 
     * @param name the name of the node.
     * @return a stream of all nodes of the Ant Colony environment (graph).
     */
    public org.graphstream.graph.Node getNode(String name) {
        return map.getNode(name);
    }

    @Override
    public void close() throws Exception {
        executor.shutdown();
    }
}
