package algorithm.firefly;

import representation.Individuum;
import representation.Node;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.function.Consumer;


import static java.lang.Math.*;

/**
 * A Firefly lives in the problem's search space, where the dimension is equal to the problem size.
 * Its position in the search space is it's tour-permutation. Movement in search space is therefore
 * represented by changing edges -> changing position = creating a new permutation.
 * It glows brighter, the smaller the permutation's cost.
 */
public class Firefly extends Individuum {

    /**
     * Attraction at distance 0
     */
    private static final int BETA_0 = 1;
    /**
     * problem dimension
     */
    private final int dimension;

    public Firefly(List<Node> nodes, Runnable evalCount) {
        super(nodes, evalCount);
        this.dimension = nodes.size();
    }

    /**
     * Makes the firefly move towards the brighter firefly.
     * The movement formula [x_i = x_i + attraction * (xj − xi) + alpha(rand − 1/2)]
     * is adapted for discrete tsp: Movement through the search space is interpreted as edge changes.
     * Depending on attraction, it adopts edges from the brighter firefly.
     * It then makes a number of random edge changes depending on alpha.
     * Only applies, if position after movement would be better than current position.
     *
     * @param brighterFirefly   firefly to move towards
     * @param gamma             environment's light absorption
     * @param random            source of random
     * @param alpha             maximum of random steps a firefly can make
     * @param increaseEvalCount for evaluation count
     */
    public void moveTowards(Firefly brighterFirefly, Random random, double gamma, int alpha, Runnable increaseEvalCount) {
        moveOnlyIfBetter(firefly -> {
            firefly.moveTowards(brighterFirefly, gamma);
            firefly.moveRandomly(random, alpha);
        }, increaseEvalCount);
    }

    /**
     * Makes the firefly do up to alpha random moves through the search space.
     * Only applies, if position after movement would be better than current position.
     *
     * @param random            source of random
     * @param alpha             maximum of random steps a firefly can make
     * @param increaseEvalCount for evaluation count
     */
    public void moveRandomly(Random random, int alpha, Runnable increaseEvalCount) {
        moveOnlyIfBetter(firefly -> firefly.moveRandomly(random, alpha), increaseEvalCount);
    }

    /**
     * Returns the fireflies' brightness which is equal to an individuum's fitness
     *
     * @return light intensity
     */
    public double getLightIntensity() {
        return getFitness();
    }

    /**
     * Updates the firefly's brightness by calculating the cost of path
     * as the brightness equals the individuum's fitness
     */
    private void updateLightIntensity() {
        calculatePathCost();
    }

    /**
     * Updates the firefly's brightness by adopting the given nodes and cost.
     *
     * @param newNodes newNodes
     * @param cost     cost of newNodes
     */
    private void updateLightIntensity(List<Node> newNodes, long cost) {
        setNodes(newNodes);
        setCost(cost);
    }

    /**
     * Moves the firefly towards a brighter firefly by adopting some of its' edges depending on attraction.
     */
    private void moveTowards(Firefly brighterFirefly, double gamma) {
        double distance = distanceInEdgesTo(brighterFirefly);
        double attractiveness = getAttraction(distance, gamma);
        long steps = round(dimension*attractiveness); // number of steps depend on attraction

        List<Node> weakNodes = getNodes();
        int stepsTaken = 0;

        /* start flying .. */
        for (int i = 0; i < weakNodes.size() && stepsTaken < steps; i++) {
            Node currentNode = weakNodes.get(i);
            Node successorNode = weakNodes.get((i + 1) % dimension); // current node's successor
            List<Node> brighterEdgeNodes = brighterFirefly.findEdgeNodes(currentNode); // find brighter firefly's edges for currentNode

            /* if brighter firefly does NOT contain currentNode-successorNode-Edge, adopt edge from brighter firefly */
            if (!brighterEdgeNodes.contains(successorNode)) {
                Node newEdgeNode = brighterEdgeNodes.get(1); // choose brighter Firefly's successor as new successor for currentNode
                int indexInWeak = weakNodes.indexOf(newEdgeNode); // find new successor in own route
                int firstEdge = Math.min(i, indexInWeak);
                int secondEdge = Math.max(i, indexInWeak);
                twoOptSwap(firstEdge, secondEdge); // swap to create currentNode-newSuccessor-Edge
                stepsTaken++;
            }
        }
    }

    /**
     * Performs movement on a clone and adopts new position if it is better than current.
     *
     * @param movement          movement to perform
     * @param increaseEvalCount for evaluation count
     */
    private void moveOnlyIfBetter(Consumer<Firefly> movement, Runnable increaseEvalCount) {
        Firefly clone = new Firefly(new ArrayList<>(getNodes()), increaseEvalCount); // save
        movement.accept(clone);
        clone.updateLightIntensity();
        if (clone.getFitness() > getFitness()) {
            updateLightIntensity(clone.getNodes(), clone.getCost());
        }
    }

    /**
     * Moves the firefly by changing up to alpha edges.
     */
    private void moveRandomly(Random random, int alpha) {
        int steps = random.nextInt(1, alpha + 1);
        for (int i = 0; i < steps; i++) {
            int geneCount = getNodes().size();
            int firstIndex = random.nextInt(geneCount - 1);
            int secondIndex = random.nextInt(firstIndex + 1, geneCount);
            twoOptSwap(firstIndex, secondIndex);
        }
    }

    /**
     * Calculates the distance to another firefly.
     * The Distance is defined as the total number of different edges between Nodes.
     * A-B and B-A count as same edge for symmetrical tsp
     * Normalizes distance in [0,10] intervall to be problem-independent and to fit attraction-function.
     *
     * @param otherFirefly other firefly
     * @return normalized distance
     */
    private double distanceInEdgesTo(Firefly otherFirefly) {
        int edgesNotInOther = 0;
        for (int i = 0; i < dimension; i++) {

            int indexOfTo = (i + 1) % dimension; // for last index, the following node is first node
            Node currentNode = getNodes().get(i);
            Node successorNode = getNodes().get(indexOfTo);

            /* if other firefly does not contain the currentNode-successorNode-Edge, distance++ */
            if (!otherFirefly.findEdgeNodes(currentNode).contains(successorNode)) {
                edgesNotInOther++;
            }
        }
        return edgesNotInOther/(double)dimension * 10; // normalized distance -> distance in intervall [0,10]
    }

    /**
     * Searches for the edge-nodes of the given node in its tour.
     *
     * @param node node
     * @return {predecessor of node, successor of node}
     */
    private List<Node> findEdgeNodes(Node node) {
        List<Node> nodes = getNodes();
        int last = nodes.size() - 1;
        int indexOfFrom = nodes.indexOf(node);
        int indexOfPrev = indexOfFrom == 0 ? last : indexOfFrom - 1;
        int indexOfSucc = indexOfFrom == last ? 0 : indexOfFrom + 1;
        return new ArrayList<>(List.of(nodes.get(indexOfPrev), nodes.get(indexOfSucc)));
    }

    /**
     * Calculates the firefly's attraction beta towards other fireflies depending
     * on their distance and the light absorption gamma.
     * Using the approximated formula beta = beta_0/(1+gamma*distance^2)
     *
     * @param distance distance to other firefly
     * @param gamma    light absorption factor
     * @return attraction
     */
    private double getAttraction(double distance, double gamma) {
        return BETA_0 /(1+(gamma * pow((distance), 2)));
    }

    /**
     * Performs the edge replacement by reversing the nodes between two edges.
     * A-B and C-D => A-C and B-D: ABEFGCD => ACGFEBD
     *
     * @param firstEdge start of first edge
     * @param secondEdge start of second edge
     */
    private void twoOptSwap(int firstEdge, int secondEdge) {
        Collections.reverse(getNodes().subList(firstEdge + 1, secondEdge + 1));
    }
}
