package algorithm.firefly;

import algorithm.Algorithm;
import output.ILogger;
import representation.Individuum;
import representation.Population;
import tsp.TSPData;

import static java.lang.Math.*;

public class FireflyAlgorithm implements Algorithm {

    private int evalCount = 0;

    private final Runnable incEvalCount = () -> evalCount++;
    private ILogger logger;
    private final int populationSize;
    /**
     * number of maximal random steps a firefly can make, value >= 1
     */
    private final int randomMovementAlpha;
    private final int maxGenerations;

    public FireflyAlgorithm(int populationSize, int randomMovementAlpha, int maxGenerations) {
        this.populationSize = populationSize;
        this.randomMovementAlpha = randomMovementAlpha;
        this.maxGenerations = maxGenerations;
    }

    @Override
    public void setLogger(ILogger logger) {
        this.logger = logger;
    }

    @Override
    public Individuum run(TSPData input) {
        double absorptionCoefficientGamma = 1.0 / sqrt(input.dimension()); //light is absorbed by air
        FireflySwarm swarm = FireflySwarm.buildRandom(input, populationSize, randomMovementAlpha, incEvalCount);
        Population population = new Population(swarm.getFireflies());

        int generation = 0;
        while (generation < maxGenerations) {
            if (logger != null){
                logger.logGeneration(generation, population, evalCount);
            }
            swarm.move(absorptionCoefficientGamma, incEvalCount);
            generation++;
        }
        return population.getBest();
    }

}
