package algorithm.firefly;

import representation.Individuum;
import tsp.TSPData;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

public class FireflySwarm {

    /**
     * number of maximal random steps a firefly will make, value >= 1
     */
    private final int alpha;
    private final Random random;
    private final List<Firefly> fireflies;

    /**
     * Builds a swarm of fireflies, in which each firefly represents a random tour.
     *
     * @param data  tsp data
     * @param size  populationsize
     * @param alpha maximum random steps a firefly will make, must be >= 0
     * @return Swarm of random fireflies.
     */
    public static FireflySwarm buildRandom(TSPData data, int size, int alpha, Runnable evalCount) {
        if (alpha < 0) throw new IllegalArgumentException("alpha must be >= 0");

        List<Firefly> fireflies = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            fireflies.add(new Firefly(data.getRandomTour(), evalCount));
        }
        return new FireflySwarm(fireflies, alpha);
    }

    private FireflySwarm(List<Firefly> fireflies, int alpha) {
        this.fireflies = fireflies;
        this.random = new Random();
        this.alpha = alpha;
    }

    public List<Individuum> getFireflies() {
        return new ArrayList<>(fireflies);
    }

    /**
     * Simulates swarm movement in which every firefly moves towards brighter fireflies
     * or randomly, if it is the brightest of the swarm.
     *
     * @param gamma light absorption of environment
     */
    public void move(double gamma, Runnable increaseEvalCount) {
        sort();
        for (int i = 0; i < fireflies.size(); i++) {
            Firefly current = fireflies.get(i);
            boolean moved = false;
            for (int j = 0; j < i; j++) { // in sorted population: only those before me are brighter than me
                Firefly other = fireflies.get(j);
                if (other.getLightIntensity() > current.getLightIntensity()) {
                    current.moveTowards(other, random, gamma, alpha, increaseEvalCount);
                    moved = true;
                }
            }
            if (!moved) { // when I am brighter than everyone else, I move randomly.
                current.moveRandomly(random, alpha, increaseEvalCount);
            }
        }
    }

    /**
     * Sorts the population by cost.
     */
    private void sort() {
        fireflies.sort(Comparator.comparingLong(Individuum::getCost));
    }

}
