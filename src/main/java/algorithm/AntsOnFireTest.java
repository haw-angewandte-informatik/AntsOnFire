package algorithm;

import algorithm.antsOnFire.AntsOnFireAlgorithm;
import algorithm.antsOnFire.AntsParameters;
import algorithm.antsOnFire.FireflySwarmParameters;
import output.CSVLogger;
import output.ConsoleLogger;
import output.ILogger;
import tsp.TSPData;
import tsp.TSPDataParser;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AntsOnFireTest {
    public static final String SET_29 = "wi29.tsp";

    public static final String SET_194 = "qa194.tsp";

    public static final String SET_734 = "uy734.tsp";

    public static final int SET_29_OPT = 27603;

    public static final int SET_194_OPT = 9352;

    public static final int SET_734_OPT = 79114;
    private static TSPData data;

    public static void main(String[] args) throws IOException {
        TSPDataParser parser = new TSPDataParser();

        var antsParameters = new AntsParameters(100, 0.5, 0.6, 1.4, true, false);
        var fireflySwarmParameters = new FireflySwarmParameters(0, 2, 5, true);

        if (args.length > 0 && args[0].equals("long")) {
            data = parser.readTSPDataFromFile(SET_734);
            CSVLogger csv = new CSVLogger("AOF_" + SET_734.split("\\.")[0] + ".csv");
            System.out.println("Starting 10 runs á 1000 generations for " + SET_734.split("\\.")[0]);
            System.out.println("Parameters: " + antsParameters + " " + fireflySwarmParameters + " Rank-based: " + 40);
            for (int i = 0; i < 10; i++) {
                csv.setRun(i);
                AntsOnFireAlgorithm algo = new AntsOnFireAlgorithm(SET_734_OPT, 1000, antsParameters,
                        fireflySwarmParameters,
                        40);
                algo.setLogger(new ConsoleLogger().andThen(csv));
                var best = algo.run(data);
                System.out.println(getTimestamp() + "Run " + i + " best result: " + best.getCost());
            }
            csv.close();
        } else if (args.length > 1 && args[0].equals("single")) {
            var run = Integer.parseInt(args[1]);
            data = parser.readTSPDataFromFile(SET_734);
            CSVLogger csv = new CSVLogger("AOF_" + SET_734.split("\\.")[0] + "_run" + run + ".csv");
            System.out.println("Starting run " + run + " with 1000 generations for " + SET_734.split("\\.")[0]);
            System.out.println("Parameters: " + antsParameters + " " + fireflySwarmParameters + " Rank-based: " + 40);

            csv.setRun(run);
            AntsOnFireAlgorithm algo = new AntsOnFireAlgorithm(SET_734_OPT, 1000, antsParameters,
                    fireflySwarmParameters,
                    40);
            algo.setLogger(csv.andThen(new ConsoleLogger()));
            var best = algo.run(data);
            System.out.println(getTimestamp() + "Run " + run + " best result: " + best.getCost());

            csv.close();
        } else {
            data = parser.readTSPDataFromFile(SET_194);
            ILogger logger = new CSVLogger("test.csv");
            for (int i = 0; i < 1; i++) {
                AntsOnFireAlgorithm algo = new AntsOnFireAlgorithm(SET_194_OPT, 200, antsParameters,
                        fireflySwarmParameters,
                        40);
                algo.setLogger(logger.andThen(new ConsoleLogger()));
                algo.run(data);
            }
            logger.close();
        }
    }

    private static final SimpleDateFormat sdf = new SimpleDateFormat("[HH-mm-ss] ");

    private static String getTimestamp() {
        return sdf.format(new Date());
    }
}
