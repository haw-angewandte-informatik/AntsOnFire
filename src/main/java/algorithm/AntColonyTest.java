package algorithm;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.DoubleStream;
import java.util.stream.Stream;

import algorithm.antColony.AntColonyAlgorithm;
import algorithm.antColony.AntColonyParameters;
import algorithm.successiveHalving.Individuum;
import algorithm.successiveHalving.SuccessiveHalving;
import output.CSVLogger;
import output.ConsoleLogger;
import output.ILogger;
import tsp.TSPData;
import tsp.TSPDataParser;

public class AntColonyTest {
    public static final String SET_194 = "qa194.tsp";

    public static final String SET_734 = "uy734.tsp";

    public static final int SET_194_OPT = 9352;

    public static final int SET_734_OPT = 79114;
    private static TSPData data;

    public static void main(String[] args) throws IOException {
        TSPDataParser parser = new TSPDataParser();
        data = parser.readTSPDataFromFile(SET_194);

        if (args.length > 0) {
            if (args[0].equals("sh")) {
                runSuccessiveHalving();
            }
        } else {
            // CSVLogger logger = new CSVLogger("ACO_" + SET_194.split("\\.")[0] + ".csv");
            ILogger logger = new ConsoleLogger();
            for (int i = 0; i < 1; i++) {
                // logger.setRun(i);

                var individuum = new Individuum(
                        new AntColonyParameters(1000, SET_194_OPT, 100, 0.4, 0.6, 1.4, true, 80).getList());
                runAntColony(logger, data, individuum);
            }
            logger.close();
        }
    }

    private static void runSuccessiveHalving() {
        var startTime = System.currentTimeMillis();
        var individuums = getHyperparameters();
        var endTime = System.currentTimeMillis();
        System.out.println("Time to enumerate: " + (endTime - startTime) + "ms");
        System.out.println("Number of individuums: " + individuums.size());

        DoubleStream generationRange = Stream.iterate(5, n -> n + 10).limit(20).mapToDouble(i -> (double) i);
        var bestHyperparamters = new SuccessiveHalving().run(individuums, generationRange, AntColonyTest::runAnts, 10,
                ind -> System.out.println("Parameters: " + AntColonyParameters.fromList(ind.values)));

        System.out.println("Winners:");
        for (int i = 0; i < bestHyperparamters.size(); i++) {
            var winnrerParams = AntColonyParameters.fromList(bestHyperparamters.get(i).values);
            System.out.println("#" + (i + 1) + ": " + bestHyperparamters.get(i).result);
            System.out.println("Parameters: " + winnrerParams);
        }
    }

    public static List<Individuum> getHyperparameters() {
        int numberOfAnts = data.dimension();
        List<Integer> numberOfAntsRange = List.of(numberOfAnts/2, numberOfAnts);
        List<Double> evaporationRange = Stream.iterate(0.1, n -> n + 0.2)
                .limit(5)
                .toList();
        List<Double> pheromoneWeightRange = Stream.iterate(0.1, n -> n + 0.5)
                .limit(4)
                .toList();
        List<Double> distanceWeightRange = Stream.iterate(0.1, n -> n + 0.5)
                .limit(4)
                .toList();
        List<Double> elitismRange = List.of(1.0);
        List<Integer> rankBaseAntSystemRange = Stream.iterate(0, n -> n + 30)
                .limit(3)
                .toList();

        List<Individuum> result = new ArrayList<>();
        for (double evaporationCoefficient : evaporationRange) {
            for (double pheromoneWeight : pheromoneWeightRange) {
                for (double distanceWeight : distanceWeightRange) {
                    for (double elitism : elitismRange) {
                        for (int rankBaseAntSystem : rankBaseAntSystemRange) {
                            for (int antCount : numberOfAntsRange) {
                                AntColonyParameters parameters = new AntColonyParameters(
                                        0,
                                        SET_194_OPT,
                                        antCount,
                                        evaporationCoefficient,
                                        pheromoneWeight,
                                        distanceWeight,
                                        elitism > 0.5,
                                        rankBaseAntSystem);
                                Individuum individuum = new Individuum(parameters.getList());
                                result.add(individuum);
                            }
                        }
                    }
                }
            }
        }
        return result;
    }

    public static double runAnts(Individuum hyperparameters) {
        var parameters = AntColonyParameters.fromList(hyperparameters.values);

        var algo = new AntColonyAlgorithm(
                SET_194_OPT,
                parameters.numberOfAnts(),
                parameters.maxGenerations(),
                parameters.evaporationCoefficient(),
                parameters.pheromoneWeight(),
                parameters.distanceWeight(),
                parameters.useElitism(),
                parameters.rankBasedAntSystem());
        try(ILogger logger = new ConsoleLogger().andThen(new CSVLogger("antColony.csv"))) {
            algo.setLogger(logger);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        var result = algo.run(data);
        return result.getCost();
    }

    public static void runAntColony(ILogger logger, TSPData data, Individuum hyperparameters) throws IOException {
        var parameters = AntColonyParameters.fromList(hyperparameters.values);

        System.out.println("Parameters: AntCount " + parameters.numberOfAnts() + " Evaporation "
                + parameters.evaporationCoefficient() + " PheromoneWeight "
                + parameters.pheromoneWeight() + " DistanceWeight "
                + parameters.distanceWeight() + " Elitism "
                + parameters.useElitism() + " RankBasedAntSystem "
                + parameters.rankBasedAntSystem());
        var algo = new AntColonyAlgorithm(
                parameters.lowerCostBound(),
                parameters.numberOfAnts(),
                parameters.maxGenerations(),
                parameters.evaporationCoefficient(),
                parameters.pheromoneWeight(),
                parameters.distanceWeight(),
                parameters.useElitism(),
                parameters.rankBasedAntSystem());
        algo.setLogger(logger);
        var result = algo.run(data);
        System.out.println("Best result: " + result.getCost());
    }
}
